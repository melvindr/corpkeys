
function getallimages(){
	$.ajax({
		  url: '/image/getall',
		  type: 'POST',
		  async: false,
		  data: '',
		  dataType: 'json',
		  success: function(data) {
		  		var str = '';
		  		$.each(data, function( index, value ) {
				  	//alert( index + ": " + value.title );

				  	str += '<tr><td>'+value.title+'</td><td><img alt="image not found" src="'+'/assets/images/'+value.filename+'" width="100" /></td><td>'+value.filename+'</td><td>'+value.dateadded+'</td><td><a href="javascript:void(0);" onclick="editimage('+value.id+');">Edit</a> | <a href="javascript:void(0);" onclick="deleteimage('+value.id+');">Delete</a></td></tr>';
				});

				$('.imgtbl tbody').html(str);
		  }
	});
}

function deleteimage(id){
	var confirm = window.confirm("Continue to delete this image?");

	if(confirm){
		$.ajax({
			  url: '/image/delete/'+id,
			  type: 'POST',
			  async: false,
			  data: '',
			  dataType: 'json',
			  success: function(data) {
			  		if(data.success == 1){
			  			getallimages();
			  			alert('Image deleted');
			  		}
			  		else{
			  			alert('There was an error. Please try again.');
			  		}
			  }
		});
	}
	
}

function editimage(id){
	$.ajax({
		  url: '/image/getbyid/'+id,
		  type: 'POST',
		  async: false,
		  data: '',
		  dataType: 'json',
		  success: function(data) {
		  		if(data[0]){
		  			$('#title').val(data[0].title);
		  			$('#id').val(data[0].id);
		  			$('#fileselect').val('');

		  			$('#myModal').modal('show');
		  		}
		  }
	});
}

function validateimage(){
	var fileSelect = document.getElementById('fileselect');

	var file = fileSelect.files;

	if(file.length <= 0){
		alert('Please select a valid image file');
		return false;
	}
	else{
		file = fileSelect.files[0];
		var fileType = file["type"];
		var ValidImageTypes = ["image/gif", "image/jpeg", "image/jpg", "image/bmp", "image/png"];
		if ($.inArray(fileType, ValidImageTypes) < 0) {
			 alert('Please select a valid image file');
		     return false;
		}
	}
}

$(document).ready(function(){
	getallimages();

	$(':file').on('change', function() {
		validateimage();
	});

	$('.btnadd').on('click', function() {
		$('#id').val('');
		document.getElementById('imgform').reset();
	});

	$('#imgform').on('submit', function(e) {
		e.preventDefault();

		if($.trim($('#title').val()) == ''){
			alert('Please enter title');
			return false;
		}

		var ret = validateimage();

		if(ret == false)
			return false;

		var form = document.getElementById('imgform');
		var fileSelect = document.getElementById('fileselect');
		var uploadButton = document.getElementById('saveimage');

		uploadButton.innerHTML = 'Uploading...';

		var files = fileSelect.files;
		var formData = new FormData();

		// Loop through each of the selected files.
		for (var i = 0; i < files.length; i++) {
		  var file = files[i];

		  // Check the file type.
		  if (!file.type.match('image.*')) {
		    continue;
		  }

		  // Add the file to the request.
		  formData.append('fileselect', file, file.name);
		}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/image/saveimage', true);

		xhr.onload = function () {
		  if (xhr.status === 200) {

		  	var urlval = '/image/insert';
		  	if($('#id').val() != '')
		  		urlval = '/image/update/'+$('#id').val();

		  	$.ajax({
				  url: urlval,
				  type: 'POST',
				  async: false,
				  data: 'title='+$('#title').val()+'&filename='+file.name,
				  dataType: 'json',
				  success: function(data) {
				  		if(data.success == 1){
				  			uploadButton.innerHTML = 'Save';
				  			getallimages();
				  			$('#myModal').modal('hide');
				  		}
				  		else{
				  			alert('There was an error saving the image.');
				  		}
				  }
			});
		  } else {
		    alert('An error occurred!');
		  }
		};

		xhr.send(formData);

	});
});