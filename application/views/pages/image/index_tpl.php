<div class="container">
  <h2>Image List</h2>
  <button type="button" class="btn btn-info btnadd" data-toggle="modal" data-target="#myModal">Add New Image</button>
  <table class="table table-striped imgtbl">
    <thead>
      <tr>
        <th>Title</th>
        <th>Thumbnail</th>
        <th>Filename</th>
        <th>Date Added</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Save Image</h4>
        </div>
        <div class="modal-body">
          <form enctype="multipart/form-data" id="imgform">
                <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />
                <input type="hidden" id="id" name="id" value="">

                <div class="form-group">
                  <label for="title">Title:</label>
                  <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group">
                  <label for="image">Image:</label>
                  <input type="file" class="form-control-file" name="fileselect" id="fileselect" aria-describedby="fileHelp">
                </div>
                
                <button type="submit" id="saveimage" class="btn btn-default saveimage">Save</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>