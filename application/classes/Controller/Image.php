<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Image extends Controller_DefaultTemplate {

	public function  __construct($request, $response) {
		parent::__construct($request, $response);
	}

	public function action_index(){
		$this->render();
	}

	public function action_saveimage()
	{
		if (!empty($_FILES["fileselect"])) {
		    $myFile = $_FILES["fileselect"];

		    if ($myFile["error"] !== UPLOAD_ERR_OK) {
		        echo "<p>An error occurred.</p>";
		        exit;
		    }

		    // ensure a safe filename
		    //$name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
		    $name = $myFile["name"];

		    // don't overwrite an existing file
		    $i = 0;
		    $parts = pathinfo($name);
		    while (file_exists(getcwd().'/assets/images/' . $name)) {
		        $i++;
		        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
		    }

		    // preserve file from temporary directory
		    $success = move_uploaded_file($myFile["tmp_name"],
		        getcwd().'/assets/images/' . $name);
		    if (!$success) { 
		        echo "<p>Unable to save file.</p>";
		        exit;
		    }

		    // set proper permissions on the new file
		    chmod(getcwd().'/assets/images/' . $name, 0644);
		}
	}

	public function action_insert(){
		$image = ORM::factory("Image");

		$image->title = $_POST['title'];
		$image->filename = $_POST['filename'];
		
		try
		{
			$image->save();

			die(json_encode(array('success' => 1)));
		}
		catch (ORM_Validation_Exception $e)
		{
			$errors = $e->errors('Image');
			die(json_encode(array('success' => 0)));
		}
	}

	public function action_getall()
	{
		$arr_images = array();
		$image = ORM::Factory("Image");
		$images = $image->select('id', 'title', 'filename', 'dateadded')->order_by('dateadded', 'DESC')->find_all();

		$inx = 0;
		foreach ($images as $img) {
			$arr_images[$inx]['id'] = $img->id;
			$arr_images[$inx]['title'] = $img->title;
			$arr_images[$inx]['filename'] = $img->filename;
			$arr_images[$inx]['dateadded'] = date("Y-m-d H:i:s", strtotime($img->dateadded));

			$inx++;
		}

		die(json_encode($arr_images));
	}

	public function action_getbyid(){
		$id = (int) $this->request->param('id');
		$image = ORM::Factory("Image", $id);

		$arr_images = array();
		if ($image->loaded())
		{
			$arr_images[0]['id'] = $image->id;
			$arr_images[0]['title'] = $image->title;
			$arr_images[0]['filename'] = $image->filename;
			$arr_images[0]['dateadded'] = date("Y-m-d H:i:s", strtotime($image->dateadded));
		}

		die(json_encode($arr_images));
	}

	public function action_update(){
		$id = (int) $this->request->param('id');
		$image = ORM::Factory("Image", $id);

		if ($image->loaded())
		{
			$image->title = $_POST['title'];
			$image->filename = $_POST['filename'];

			try
			{
				$image->save();
				die(json_encode(array('success' => 1)));
			}
			catch (ORM_Validation_Exception $e)
			{
				$errors = $e->errors('Image');
				die(json_encode(array('success' => 0)));
			}
		}
	}

	public function action_delete(){
		$id = (int) $this->request->param('id');
		$image = ORM::Factory("Image", $id);

		if ($image->loaded())
		{
			try
			{
				if(file_exists(getcwd().'/assets/images/'.$image->filename) && trim($image->filename) != '')
					unlink(getcwd().'/assets/images/'.$image->filename);

				$image->delete();
				die(json_encode(array('success' => 1)));
			}
			catch (ORM_Validation_Exception $e)
			{
				$errors = $e->errors('Image');

				die(json_encode(array('success' => 0)));
			}
		}
		else{
			die(json_encode(array('success' => 0)));
		}
	}

} // End Image
